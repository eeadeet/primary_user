package org.project.DAO.Impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.project.dao.Impl.RefrigeratorDao;
import org.project.entity.Impl.Refrigerator;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RefrigeratorDaoTest {

    private RefrigeratorDao refrigeratorDAO;

    @BeforeEach
    void setUp() {
        refrigeratorDAO = new RefrigeratorDao();
    }

    @AfterEach
    void tearDown() {
        refrigeratorDAO = null;
    }

    @Test
    void loadData() {
        String reader = "src/main/resources/Path/Refrigerator.csv";
        refrigeratorDAO.loadData(reader);

        List<Refrigerator> daoList = refrigeratorDAO.daoList;

        // Check the size of the loaded data
        assertEquals(10, daoList.size());

        // Check the values of the first product
        Refrigerator firstProduct = daoList.get(0);
        assertEquals(21, firstProduct.getId());
        assertEquals("Refrigerator", firstProduct.getName());
        assertEquals("NORCOLD", firstProduct.getBrand());
        assertEquals(4, firstProduct.getQuantity());
        assertEquals("18", firstProduct.getCapacity());
        assertEquals("69D x 36W x 24H", firstProduct.getProduct_dimensions());
        assertEquals(6553, firstProduct.getPrice(), 0.001);
    }
}
