package org.project.DAO.Impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.project.dao.Impl.TvDao;
import org.project.entity.Impl.TV;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TvDaoTest {

    private TvDao tvDAO;

    @BeforeEach
    void setUp() {
        tvDAO = new TvDao();
    }

    @AfterEach
    void tearDown() {
        tvDAO = null;
    }

    @Test
    void loadData() {
        String reader = "src/main/resources/Path/TV.csv";
        tvDAO.loadData(reader);

        List<TV> daoList = tvDAO.daoList;

        // Check the size of the loaded data
        assertEquals(10, daoList.size());

        // Check the values of the first product
        TV firstProduct = daoList.get(0);
        assertEquals(31, firstProduct.getId());
        assertEquals("TV", firstProduct.getName());
        assertEquals("VIZIO", firstProduct.getBrand());
        assertEquals(10, firstProduct.getQuantity());
        assertEquals("LCD", firstProduct.getDisplay());
        assertEquals("1080p", firstProduct.getResolution());
        assertEquals("60Hz", firstProduct.getRefresh_rate());
        assertEquals("40", firstProduct.getScreen_size());
        assertEquals(126, firstProduct.getPrice());
    }
}
