package org.project.DAO.Impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.project.dao.Impl.VacuumCleanerDao;
import org.project.entity.Impl.VacuumCleaner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Vacuum_CleanerDaoTest {

    private VacuumCleanerDao vacuumCleanerDAO;

    @BeforeEach
    void setUp() {
        vacuumCleanerDAO = new VacuumCleanerDao();
    }

    @AfterEach
    void tearDown() {
        vacuumCleanerDAO = null;
    }

    @Test
    void loadData() {
        String reader = "src/main/resources/Path/Vacuum Cleaner.csv";
        vacuumCleanerDAO.loadData(reader);

        List<VacuumCleaner> daoList = vacuumCleanerDAO.daoList;

        // Check the size of the loaded data
        assertEquals(10, daoList.size());

        // Check the values of the first product
        VacuumCleaner firstProduct = daoList.get(0);
        assertEquals(41, firstProduct.getId());
        assertEquals("Vacuum-Cleaner", firstProduct.getName());
        assertEquals("SharkNinja", firstProduct.getBrand());
        assertEquals(13, firstProduct.getQuantity());
        assertEquals("17 lbs", firstProduct.getWeight());
        assertEquals("Not listed", firstProduct.getCord_length());
        assertEquals("Bagless", firstProduct.getBag_or_bagless());
        assertEquals(399, firstProduct.getPrice());
    }
}
