package org.project.DAO.Impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.project.dao.Impl.HairDryerDao;
import org.project.entity.Impl.HairDryer;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Hair_DryerDaoTest {
    private HairDryerDao hairDryerDAO;

    @BeforeEach
    void setUp() {
        hairDryerDAO = new HairDryerDao();
    }

    @AfterEach
    void tearDown() {
        hairDryerDAO = null;
    }

    @Test
    void loadData() {
        hairDryerDAO.loadData("src/main/resources/Path/HAIR DRYER.csv");

        List<HairDryer> daoList = hairDryerDAO.daoList;

        // Assert that the loaded data has the expected number of entries
        assertEquals(10, daoList.size());

        // Assert specific values for one of the loaded hair dryers
        HairDryer hairDryer = daoList.get(0);
        assertEquals(11, hairDryer.getId());
        assertEquals("DRYER", hairDryer.getName());
        assertEquals("ghd", hairDryer.getBrand());
        assertEquals(13, hairDryer.getQuantity());
        assertEquals("No", hairDryer.getIonicTech());
        assertEquals("460", hairDryer.getWattage());
        assertEquals("672", hairDryer.getWeight());
        assertEquals(379.0, hairDryer.getPrice(), 0.01);
    }
}
