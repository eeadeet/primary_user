package org.project.DAO.Impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.project.dao.Impl.AirConditionerDao;
import org.project.entity.Impl.AirConditioner;

import static org.junit.jupiter.api.Assertions.*;

class Air_ConditionerDaoTest {

    private AirConditionerDao airConditionerDAO;

    @BeforeEach
    void setUp() {
        airConditionerDAO = new AirConditionerDao();
    }

    @AfterEach
    void tearDown() {
        airConditionerDAO = null;
    }

    @Test
    void loadData() {
        // Arrange
        String csvFilePath = "src/main/resources/Path/Air Conditioners.csv";

        // Act
        airConditionerDAO.loadData(csvFilePath);

        // Assert
        assertNotNull(airConditionerDAO.daoList);
        assertFalse(airConditionerDAO.daoList.isEmpty());
        assertEquals(10, airConditionerDAO.daoList.size());

        // Additional assertions
        AirConditioner product1 = airConditionerDAO.daoList.get(0);
        assertEquals(1, product1.getId());
        assertEquals("Conditioner", product1.getName());
        assertEquals("Whynter", product1.getBrand());
        assertEquals(23, product1.getQuantity());
        assertEquals("500", product1.getCapacity());
        assertEquals("14000", product1.getCooling_power());
        assertEquals("16D x 19W x 35H", product1.getProduct_dimensions());
        assertEquals(700, product1.getPrice());

        // Add more assertions based on your requirements
    }
}
