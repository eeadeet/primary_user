package org.project.dao.Impl;

import org.project.dao.DaoInterface;
import org.project.entity.Impl.Refrigerator;

import java.io.BufferedReader;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class RefrigeratorDao implements DaoInterface {
    public List<Refrigerator> daoList = new ArrayList<>();




    public void loadData(String reader) {
        try (BufferedReader br = Files.newBufferedReader(Path.of(reader))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                int id = Integer.parseInt(values[0]);
                String name = values[1];
                String brand = values[2];
                int quantity = Integer.parseInt(values[3]);
                String capacity = values[4];
                String product_dimensions = values[5];
                double price = Double.parseDouble(values[6]);

                Refrigerator product = new Refrigerator(id, name, brand, quantity, capacity, product_dimensions, (int) price);
                daoList.add(product);

            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
