package org.project.dao.Impl;

import org.project.dao.DaoInterface;
import org.project.entity.Impl.AirConditioner;
import java.io.BufferedReader;

import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class AirConditionerDao implements DaoInterface {
    public List<AirConditioner> daoList = new ArrayList<>();



    public void loadData(String reader) {
        try (BufferedReader br = Files.newBufferedReader(Path.of(reader))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                int id = Integer.parseInt(values[0]);
                String name = values[1];
                String brand = values[2];
                int quantity = Integer.parseInt(values[3]);
                String capacity = values[4];
                String cooling_power = values[5];
                String product_dimensions = values[6];
                double price = Double.parseDouble(values[7]);
                AirConditioner product = new AirConditioner(id, name, brand, quantity, capacity, cooling_power, product_dimensions, (int) price);
                daoList.add(product);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
