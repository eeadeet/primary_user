package org.project.dao.Impl;

import org.project.dao.DaoInterface;
import org.project.entity.Impl.HairDryer;
import java.io.BufferedReader;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class HairDryerDao implements DaoInterface {
    public List<HairDryer> daoList = new ArrayList<>();

    public void loadData(String reader) {
        try (BufferedReader br = Files.newBufferedReader(Path.of(reader))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                int id = Integer.parseInt(values[0]);
                String name = values[1];
                String brand = values[2];
                int quantity = Integer.parseInt(values[3]);
                String ionicTech = values[4];
                String wattage = values[5];
                String weight = values[6];
                double price = Double.parseDouble(values[7]);

                HairDryer product = new HairDryer(id, name, brand, quantity, ionicTech, wattage, weight, price);
                daoList.add(product);

        }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
