package org.project.dao.Impl;

import org.project.admin.Admin;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AdminDao {

    private static final String CSV_FILE_PATH = "src/main/resources/adminData.csv";

    public List<Admin> loadAdminData() {
        List<Admin> admins = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(CSV_FILE_PATH))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",");
                String username = data[0].trim();
                String password = data[1].trim();
                admins.add(new Admin(username, password));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return admins;
    }

    public void saveAdminData(List<Admin> admins) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(CSV_FILE_PATH))) {
            for (Admin admin : admins) {
                writer.write(admin.getUsername() + "," + admin.getPassword());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
