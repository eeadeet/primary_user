package org.project.dao.Impl;

import org.project.dao.DaoInterface;
import org.project.entity.Impl.TV;
import java.io.BufferedReader;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class TvDao implements DaoInterface {
    public List<TV> daoList = new ArrayList<>();

    public void loadData(String reader) {
        try (BufferedReader br = Files.newBufferedReader(Path.of(reader))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                int id = Integer.parseInt(values[0]);
                String name = values[1];
                String brand = values[2];
                int quantity = Integer.parseInt(values[3]);
                String display = values[4];
                String resolution = values[5];
                String refresh_rate = values[6];
                String screen_size = values[7];
                int price = Integer.parseInt(values[8]);

                TV product = new TV(id, name, brand, quantity, display, resolution, refresh_rate, screen_size, price);
                daoList.add(product);

            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
