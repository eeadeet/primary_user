package org.project.admin;

import java.util.Scanner;

    public class Admin {
        private  String username;
        private  String password;

        public Admin(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public boolean authenticate(String enteredUsername, String enteredPassword) {
            return username.equals(enteredUsername) && password.equals(enteredPassword);
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
