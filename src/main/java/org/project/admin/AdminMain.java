// AdminMain.java
package org.project.admin;

import org.project.dao.Impl.AdminDao;
import org.project.service.Impl.AdminService;

import java.util.Scanner;

public class AdminMain {

    public static void adminMenu() {
        Scanner scanner = new Scanner(System.in);
        AdminDao adminDao = new AdminDao();
        AdminService adminService = new AdminService(adminDao);

        // Set your admin username and password
        Admin admin = new Admin("admin", "admin");

        System.out.println("Welcome to Admin Panel");
        System.out.print("Enter username: ");
        String enteredUsername = scanner.nextLine();

        System.out.print("Enter password: ");
        String enteredPassword = scanner.nextLine();

        if (admin.authenticate(enteredUsername, enteredPassword)) {
            System.out.println("Authentication successful!");

            // Menu for admin actions
            while (true) {
                System.out.println("\nAdmin Menu:");
                System.out.println("1. Add element");
                System.out.println("2. Delete element");
                System.out.println("3. Add category");
                System.out.println("4. Exit");

                System.out.print("Enter your choice: ");
                int choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        // Add element logic
                        System.out.println("Enter details to add a new element:");
                        System.out.print("ID: ");
                        int id = scanner.nextInt();
                        System.out.print("Name: ");
                        String name = scanner.next();
                        System.out.print("Brand: ");
                        String brand = scanner.next();
                        System.out.print("Quantity: ");
                        int quantity = scanner.nextInt();
                        System.out.print("Price: ");
                        double price = scanner.nextDouble();

                        // Use your service methods to add the new element
                        adminService.addAdmin(id, name, brand, quantity, price);
                        System.out.println("Element added successfully.");
                        break;
                    case 2:
                        // Delete element logic
                        System.out.println("Enter the ID of the element to delete:");
                        int elementIdToDelete = scanner.nextInt();

                        // Use your service methods to delete the element
                        if (adminService.deleteAdmin(String.valueOf(elementIdToDelete))) {
                            System.out.println("Element deleted successfully.");
                        } else {
                            System.out.println("Element not found.");
                        }
                        break;
                    case 3:
                        // Add category logic
                        System.out.println("Enter the name of the new category:");
                        String newCategoryName = scanner.next();

                        // Use your service methods to add the new category
                        adminService.addCategory(newCategoryName);
                        System.out.println("Category added successfully.");
                        break;
                    case 4:
                        System.out.println("Exiting Admin Panel. Goodbye!");
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                        break;
                }
            }
        } else {
            System.out.println("Authentication failed. Exiting.");
        }
    }
}
