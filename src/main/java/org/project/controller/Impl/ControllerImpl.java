package org.project.controller.Impl;
import org.project.controller.Controller;
import org.project.service.Impl.*;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class ControllerImpl implements Controller {

    private void clearAir(){
        airConditioner.clear();
    }private void clearHair(){
        hair_dryer.clear();
    }private void clearRef(){
        refrigerator.clear();
    }private void clearTv(){
        tv.clear();
    }private void clearVac(){
        vacuum_cleaner.clear();
    }
    private final AirConditionerService airConditioner;
    private final HairDryerService hair_dryer;
    private final RefrigeratorService refrigerator;
    private final TvService tv;
    private final VacuumCleanerService vacuum_cleaner;


    public ControllerImpl(AirConditionerService airConditioner, HairDryerService hair_dryer, RefrigeratorService refrigerator, TvService tvService, VacuumCleanerService vacuum_cleaner) throws FileNotFoundException {
        this.airConditioner = airConditioner;
        this.hair_dryer = hair_dryer;
        this.refrigerator = refrigerator;
        this.tv = tvService;
        this.vacuum_cleaner = vacuum_cleaner;
    }

    public void view() {
            //Project
        System.out.println("""
            --------------------------------------------
            Project Name: Household appliances warehouse
            Start: 22.05.23\s
            End:   09.06.23
            --------------------------------------------
                    """);
            // Author
            System.out.println("""
            --------------------------------------------
            Name: Sardor
            Surname: Xabibullayev
            AkA:SaM
            Email: Sardor_Xabibullayev@student.itpu.uz
            --------------------------------------------      \s
                     """);
        Scanner scanner = new Scanner(System.in);

            while (true) {
                try {
                System.out.print("""
                        ========== Product Search ==========
                                Enter a search term
                        1.All
                        2.ID
                        3.Name
                        4.Brand
                        5.Price
                        0.Exit
                        
                        Type a number:\040""");


                int searchTerm = scanner.nextInt();
                switch (searchTerm) {
                    case 1 -> showAll();
                    case 2 -> showID(scanner);
                    case 3 -> showName(scanner);
                    case 4 -> showBrand(scanner);
                    case 5 -> showPrice(scanner);
                    case 0 -> {
                        System.out.println("Program is Over.");
                        return;

                    }
                    default -> System.out.println("NOT FOUND" + "\n");
                }



        } catch (InputMismatchException e) {
            System.err.println("\nInvalid input!");
            scanner.nextLine();
        }
            }
    }


    @Override
    public void showAll() {
        airConditioner.getAll();
        clearAir();
        hair_dryer.getAll();
        clearHair();
        refrigerator.getAll();
        clearRef();
        tv.getAll();
        clearTv();
        vacuum_cleaner.getAll();
        clearVac();
    }

    @Override
    public void showID(Scanner searchTerm) {
        System.out.print("Enter the ID: ");
        int ID = searchTerm.nextInt();
        airConditioner.getId(ID);
        clearAir();
        hair_dryer.getId(ID);
        clearHair();
        refrigerator.getId(ID);
        clearRef();
        tv.getId(ID);
        clearTv();
        vacuum_cleaner.getId(ID);
        clearVac();
    }

    @Override
    public void showName(Scanner searchTerm){
        try {
            System.out.print("Enter the Name: ");
            String Name = searchTerm.next();
            if (Name.matches(".*\\d+.*")) {
                throw new IllegalArgumentException("Invalid");
            }
            searchTerm.nextLine();
            airConditioner.getName(Name);
            clearAir();
            hair_dryer.getName(Name);
            clearHair();
            refrigerator.getName(Name);
            clearRef();
            tv.getName(Name);
            clearTv();
            vacuum_cleaner.getName(Name);
            clearVac();
        } catch (IllegalArgumentException e){
            System.err.println("Invalid");
        }
    }

    @Override
    public void showBrand(Scanner searchTerm){
        try {
            System.out.print("Enter the Brand: ");
            String Brand = searchTerm.next();
            if (Brand.matches(".*\\d+.*")) {
                throw new IllegalArgumentException("Invalid");
            }
            searchTerm.nextLine();
            airConditioner.getBrand(Brand);
            clearAir();
            hair_dryer.getBrand(Brand);
            clearHair();
            refrigerator.getBrand(Brand);
            clearRef();
            tv.getBrand(Brand);
            clearTv();
            vacuum_cleaner.getBrand(Brand);
            clearVac();
        } catch (IllegalArgumentException e){
            System.err.println("Invalid");
        }
    }

    @Override
    public void showPrice(Scanner searchTerm){
        try {
            System.out.print("Enter the Price: ");
            double Price = searchTerm.nextDouble();
            airConditioner.getPrice(Price);
            clearAir();
            hair_dryer.getPrice(Price);
            clearHair();
            refrigerator.getPrice(Price);
            clearRef();
            tv.getPrice(Price);
            clearTv();
            vacuum_cleaner.getPrice(Price);
            clearVac();
        }catch (IllegalArgumentException e){
            System.err.println("Invalid");
        }
    }



}