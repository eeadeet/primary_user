package org.project.service.Impl;
import org.project.dao.Impl.TvDao;
import org.project.entity.Impl.Products;
import org.project.entity.Impl.TV;
import org.project.service.service;
import java.io.FileNotFoundException;

import java.util.List;

public class TvService implements service {
    public String csvFile  = "src/main/resources/Path/TV.csv";

    private TvDao tvDAO;
    List<TV> csvProducts;

    public TvService() throws FileNotFoundException {
        this.tvDAO = new TvDao();
        this.csvProducts = tvDAO.daoList;
    }


    @Override
    public List<TV> getAll() {
        tvDAO.loadData(csvFile);
        System.out.printf("| %-1s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", "ID", "Name", "Brand", "Quantity", "Display",
                "Resolution", "Refresh_rate","Price");
        List<TV> products = csvProducts;
        for (TV product : products) {
            System.out.printf("| %-2s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", product.getId(), product.getName(),
                    product.getBrand(),product.getQuantity(),product.getDisplay(),
                    product.getResolution(),product.getRefresh_rate(),product.getPrice());

        }
        return null;
    }

    @Override
    public TV getId(int id) {
        tvDAO.loadData(csvFile);
        List<TV> products = csvProducts;
        for (TV product : products){
            if (product.getId() == id){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getName(String name) {
        tvDAO.loadData(csvFile);
        List<TV> products = csvProducts;
        for (TV product : products){
            if (product.getName().equalsIgnoreCase(name)){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getBrand(String brand) {
        tvDAO.loadData(csvFile);
        List<TV> products = csvProducts;
        for (TV product : products) {
            if (product.getBrand().equalsIgnoreCase(brand)) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getQuantity(int quantity) {
        tvDAO.loadData(csvFile);
        List<TV> products = csvProducts;
        for (TV product : products){
            if (product.getQuantity() == quantity){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getPrice(double price) {
        tvDAO.loadData(csvFile);
        List<TV> products = csvProducts;
        for (TV product : products){
            if (product.getPrice() == price){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public void clear() {
        csvProducts.clear();
    }
}


