package org.project.service.Impl;
import org.project.dao.Impl.HairDryerDao;

import org.project.entity.Impl.HairDryer;
import org.project.entity.Impl.Products;
import org.project.service.service;
import java.io.FileNotFoundException;

import java.util.List;

public class HairDryerService implements service {
    public String csvFile = "src/main/resources/Path/HAIR DRYER.csv";
    private HairDryerDao hair_dryerDao;
    List<HairDryer> csvProducts;

    public HairDryerService() throws FileNotFoundException {
        this.hair_dryerDao = new HairDryerDao();
        this.csvProducts = hair_dryerDao.daoList;
    }

    @Override
    public List<HairDryer> getAll() {
        hair_dryerDao.loadData(csvFile);
        System.out.printf("| %-1s | %-11s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", "ID", "Name", "Brand", "Quantity", "IonicTech",
                "Wattage", "Weight", "Price");
        List<HairDryer> products = csvProducts;
        for (HairDryer product : products) {
            System.out.printf("| %-2s | %-11s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", product.getId(), product.getName(),
                    product.getBrand(),product.getQuantity(),product.getIonicTech(),
                    product.getWattage(),product.getWeight(),product.getPrice());
        }
        return null;
    }


    @Override
    public HairDryer getId(int id) {
        hair_dryerDao.loadData(csvFile);
        List<HairDryer> products = csvProducts;
        for (HairDryer product : products){
            if (product.getId() == id){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getName(String name) {
        hair_dryerDao.loadData(csvFile);
        List<HairDryer> products = csvProducts;
        for (HairDryer product : products){
            if (product.getName().equalsIgnoreCase(name)){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getBrand(String brand) {
        hair_dryerDao.loadData(csvFile);
        List<HairDryer> products = csvProducts;
        for (HairDryer product : products) {
            if (product.getBrand().equalsIgnoreCase(brand)) {
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getQuantity(int quantity) {
        hair_dryerDao.loadData(csvFile);
        List<HairDryer> products = csvProducts;
        for (HairDryer product : products){
            if (product.getQuantity() == quantity){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getPrice(double price) {
        hair_dryerDao.loadData(csvFile);
        List<HairDryer> products = csvProducts;
        for (HairDryer product : products){
            if (product.getPrice() == price){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public void clear() {
        csvProducts.clear();
    }
}
