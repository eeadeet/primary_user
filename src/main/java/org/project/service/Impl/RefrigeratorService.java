package org.project.service.Impl;

import org.project.dao.Impl.RefrigeratorDao;

import org.project.entity.Impl.Products;
import org.project.entity.Impl.Refrigerator;
import org.project.service.service;

import java.io.FileNotFoundException;

import java.util.List;

public class RefrigeratorService implements service {
    public String csvFile = "src/main/resources/Path/Refrigerator.csv";

    private RefrigeratorDao refrigeratorDAO;
    List<Refrigerator> csvProducts;

    public RefrigeratorService() throws FileNotFoundException {
        this.refrigeratorDAO = new RefrigeratorDao();
        this.csvProducts = refrigeratorDAO.daoList;
    }


    @Override
    public List<Refrigerator> getAll() {
        refrigeratorDAO.loadData(csvFile);
        System.out.printf("| %-1s | %-12s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", "ID", "Name", "Brand", "Quantity", "Capacity",
                "Product_dimensions", "Price");
        List<Refrigerator> products = csvProducts;
        for (Refrigerator product : products) {
            System.out.printf("| %-2s | %-10s | %-10s | %-10s | %-10s | %-18s | %-10s |%n", product.getId(), product.getName(),
                    product.getBrand(),product.getQuantity(),product.getCapacity(),
                    product.getProduct_dimensions(),product.getPrice());
        }
        return null;
    }

    @Override
    public Refrigerator getId(int id) {
        refrigeratorDAO.loadData(csvFile);
        List<Refrigerator> products = csvProducts;
        for (Refrigerator product : products){
            if (product.getId() == id){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getName(String name) {
        refrigeratorDAO.loadData(csvFile);
        List<Refrigerator> products = csvProducts;
        for (Refrigerator product : products){
            if (product.getName().equalsIgnoreCase(name)){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getBrand(String brand) {
        refrigeratorDAO.loadData(csvFile);
        List<Refrigerator> products = csvProducts;
        for (Refrigerator product : products) {
            if (product.getBrand().equalsIgnoreCase(brand)) {
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getQuantity(int quantity) {
        refrigeratorDAO.loadData(csvFile);
        List<Refrigerator> products = csvProducts;
        for (Refrigerator product : products){
            if (product.getQuantity() == quantity){
                System.out.println(product);

            }
        }
        return null;
    }

    @Override
    public Products getPrice(double price) {
        refrigeratorDAO.loadData(csvFile);
        List<Refrigerator> products = csvProducts;
        for (Refrigerator product : products){
            if (product.getPrice() == price){
                System.out.println(product);
            }
        }
        return null;

    }
    @Override
    public void clear() {
        csvProducts.clear();
    }
}


