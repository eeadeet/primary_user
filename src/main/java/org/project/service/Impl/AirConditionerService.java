package org.project.service.Impl;
import org.project.dao.Impl.AirConditionerDao;
import org.project.entity.Impl.AirConditioner;
import org.project.entity.Impl.Products;
import org.project.service.service;
import java.io.FileNotFoundException;


import java.util.List;

public class AirConditionerService implements service {

    public String csvFile = "src/main/resources/Path/Air Conditioners.csv";
    private AirConditionerDao airConditionerDAO;
    List<AirConditioner> csvProducts;

    public AirConditionerService() throws FileNotFoundException {
        this.airConditionerDAO = new AirConditionerDao();
        this.csvProducts = airConditionerDAO.daoList;
    }

    @Override
    public List<AirConditioner> getAll() {
        airConditionerDAO.loadData(csvFile);
        System.out.printf("| %-1s | %-11s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", "ID", "Name", "Brand", "Quantity", "Capacity",
                "Cooling_power", "Product_dimensions", "Price");
        List<AirConditioner> products = csvProducts;
        for (AirConditioner product : products) {
            System.out.printf("| %-2s | %-10s | %-10s | %-10s | %-10s | %-13s | %-18s | %-10s |%n", product.getId(), product.getName(),
                    product.getBrand(),product.getQuantity(),product.getCapacity(),
                    product.getCooling_power(),product.getProduct_dimensions(),product.getPrice());
        }
        return null;
    }

    @Override
    public AirConditioner getId(int id) {
        airConditionerDAO.loadData(csvFile);
        List<AirConditioner> products = csvProducts;
        for (AirConditioner product : products){
        if (product.getId() == id){
            System.out.println(product);
        }
    }
    return null;
    }

    @Override
    public Products getName(String name) {
        airConditionerDAO.loadData(csvFile);
        List<AirConditioner> products = csvProducts;
        for (AirConditioner product : products){
            if (product.getName().equalsIgnoreCase(name)){
                System.out.println(product);


            }
        }
        return null;
    }

    @Override
    public Products getBrand(String brand) {
        airConditionerDAO.loadData(csvFile);
        List<AirConditioner> products = csvProducts;
        for (AirConditioner product : products){
            if (product.getBrand().equalsIgnoreCase(brand)){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getQuantity(int quantity) {
        airConditionerDAO.loadData(csvFile);
        List<AirConditioner> products = csvProducts;
        for (AirConditioner product : products){
            if (product.getQuantity() == quantity){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getPrice(double price) {
        airConditionerDAO.loadData(csvFile);
        List<AirConditioner> products = csvProducts;
        for (AirConditioner product : products){
            if (product.getPrice() == price){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public void clear() {
        csvProducts.clear();
    }
}
