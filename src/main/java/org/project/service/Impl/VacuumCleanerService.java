package org.project.service.Impl;


import org.project.dao.Impl.VacuumCleanerDao;
import org.project.entity.Impl.Products;

import org.project.entity.Impl.VacuumCleaner;
import org.project.service.service;

import java.io.FileNotFoundException;


import java.util.List;

public class VacuumCleanerService implements service {
    public String csvFile = "src/main/resources/Path/Vacuum Cleaner.csv";
    private VacuumCleanerDao vacuum_cleanerDao;
    List<VacuumCleaner> csvProducts;

    public VacuumCleanerService() throws FileNotFoundException {
        this.vacuum_cleanerDao = new VacuumCleanerDao();
        this.csvProducts = vacuum_cleanerDao.daoList;
    }


    @Override
    public List<VacuumCleaner> getAll() {
        vacuum_cleanerDao.loadData(csvFile);
        System.out.printf("| %-1s | %-14s | %-10s | %-10s | %-9s | %-10s | %-7s | %-9s |%n", "ID", "Name", "Brand", "Quantity", "Weight",
                "Cord_length", "Bag=bagless", "Price");
        List<VacuumCleaner> products = csvProducts;
        for (VacuumCleaner product : products) {
            System.out.printf("| %-2s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s | %-10s |%n", product.getId(), product.getName(),
                    product.getBrand(),product.getQuantity(),product.getWeight(),
                    product.getCord_length(),product.getBag_or_bagless(),product.getPrice());
        }
        return null;
    }

    @Override
    public VacuumCleaner getId(int id) {
        vacuum_cleanerDao.loadData(csvFile);
        List<VacuumCleaner> products = csvProducts;
        for (VacuumCleaner product : products){
            if (product.getId() == id){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getName(String name) {
        vacuum_cleanerDao.loadData(csvFile);
        List<VacuumCleaner> products = csvProducts;
        for (VacuumCleaner product : products){
            if (product.getName().equalsIgnoreCase(name)){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getBrand(String brand) {
        vacuum_cleanerDao.loadData(csvFile);
        List<VacuumCleaner> products = csvProducts;
        for (VacuumCleaner product : products) {
            if (product.getBrand().equalsIgnoreCase(brand)) {
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getQuantity(int quantity) {
        vacuum_cleanerDao.loadData(csvFile);
        List<VacuumCleaner> products = csvProducts;
        for (VacuumCleaner product : products){
            if (product.getQuantity() == quantity){
                System.out.println(product);
            }
        }
        return null;
    }

    @Override
    public Products getPrice(double price) {
        vacuum_cleanerDao.loadData(csvFile);
        List<VacuumCleaner> products = csvProducts;
        for (VacuumCleaner product : products){
            if (product.getPrice() == price){
                System.out.println(product);
            }
        }
        return null;
    }
    @Override
    public void clear() {
        csvProducts.clear();
    }
}
