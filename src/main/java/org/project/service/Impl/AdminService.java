package org.project.service.Impl;

import org.project.admin.Admin;
import org.project.dao.Impl.AdminDao;

import java.util.List;

public class AdminService {
    private AdminDao adminDao;

    public AdminService(AdminDao adminDao) {
        this.adminDao = adminDao;

    }

    public List<Admin> getAllAdmins() {
        return adminDao.loadAdminData();
    }

    public void addAdmin(Admin admin) {
        List<Admin> admins = adminDao.loadAdminData();
        admins.add(admin);
        adminDao.saveAdminData(admins);
    }

    public boolean deleteAdmin(String username) {
        List<Admin> admins = adminDao.loadAdminData();
        for (Admin admin : admins) {
            if (admin.getUsername().equals(username)) {
                admins.remove(admin);
                adminDao.saveAdminData(admins);
                return true;
            }
        }
        return false;
    }

    public void addCategory(String categoryName) {

    }

    public void addAdmin(int id, String name, String brand, int quantity, double price) {
    }
}
