package org.project.service;

import org.project.entity.Impl.Products;

import java.util.List;

public interface service <T extends Products> {
    List getAll();


    Products getId(int id);

    Products getName(String name);

    Products getBrand(String brand);

    Products getQuantity(int quantity);

    Products getPrice(double price);
    void clear();
}
