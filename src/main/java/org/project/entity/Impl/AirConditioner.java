package org.project.entity.Impl;

import java.util.Objects;

public class AirConditioner extends Products {

    public AirConditioner(int id, String name, String brand, int quantity, double price) {
        super(id, name, brand, quantity, price);
    }

    private String capacity;
    private String cooling_power;
    private String product_dimensions;


    public AirConditioner(int id, String name, String brand, int quantity, String capacity, String cooling_power, String product_dimensions, double price) {
        super(id, name, brand, quantity, price);

        this.capacity = capacity;
        this.cooling_power = cooling_power;
        this.product_dimensions = product_dimensions;

    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getCooling_power() {
        return cooling_power;
    }

    public void setCooling_power(String cooling_power) {
        this.cooling_power = cooling_power;
    }

    public String getProduct_dimensions() {
        return product_dimensions;
    }

    public void setProduct_dimensions(String product_dimensions) {
        this.product_dimensions = product_dimensions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AirConditioner that = (AirConditioner) o;
        return capacity.equals(that.capacity) && cooling_power.equals(that.cooling_power) && product_dimensions.equals(that.product_dimensions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, cooling_power, product_dimensions);
    }

    @Override
    public String toString() {
        return "\nConditioner \uD83D\uDD3B\n"+
                "ID => " + super.getId() + "\n" +
                "Name => " + super.getName() + "\n" +
                "Brand => " + super.getBrand() + "\n" +
                "Quantity => " + super.getQuantity() + "\n" +
                "Capacity => " + capacity + "\n" +
                "Cooling Power => " + cooling_power + "\n" +
                "Product Dimensions => "  + product_dimensions + "\n" +
                "Price => " + super.getPrice();
    }
}


