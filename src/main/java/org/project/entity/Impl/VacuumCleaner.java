package org.project.entity.Impl;

import java.util.Objects;

public class VacuumCleaner extends Products {
    public VacuumCleaner(int id, String name, String brand, int quantity, double price) {
        super(id, name, brand, quantity, price);
    }


    private String weight;
    private String cord_length;
    private String bag_or_bagless;



    public VacuumCleaner(int id, String name, String brand, int quantity, String weight, String cord_length, String bag_or_bagless, double price) {
        super(id, name, brand, quantity, price);

        this.weight = weight;
        this.cord_length = cord_length;
        this.bag_or_bagless = bag_or_bagless;

    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCord_length() {
        return cord_length;
    }

    public void setCord_length(String cord_length) {
        this.cord_length = cord_length;
    }

    public String getBag_or_bagless() {
        return bag_or_bagless;
    }

    public void setBag_or_bagless(String bag_or_bagless) {
        this.bag_or_bagless = bag_or_bagless;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VacuumCleaner that = (VacuumCleaner) o;
        return weight.equals(that.weight) && cord_length.equals(that.cord_length) && bag_or_bagless.equals(that.bag_or_bagless);
    }

    @Override
    public int hashCode() {
        return Objects.hash(weight, cord_length, bag_or_bagless);
    }

    @Override
    public String toString() {
        return "\nCleaner \uD83D\uDD3B\n"+
                "ID => " + super.getId() + "\n" +
                "Name => " + super.getName() + "\n" +
                "Brand => " + super.getBrand() + "\n" +
                "Quantity => " + super.getQuantity() + "\n" +
                "Weight => " + weight + "\n" +
                "Cord Length => " + cord_length + "\n" +
                "Bag or Bagless => " + bag_or_bagless + "\n" +
                "Price => " + super.getPrice();
    }
}
