package org.project.entity.Impl;

import java.util.Objects;

public class HairDryer extends Products {

    public HairDryer(int id, String name, String brand, int quantity, double price) {
        super(id, name, brand, quantity, price);
    }

    private String ionicTech;
    private String wattage;
    private String weight;



    public HairDryer(int id, String name, String brand, int quantity, String ionicTech, String wattage, String weight, double price) {
        super(id, name, brand, quantity, price);

        this.ionicTech=ionicTech;
        this.wattage = wattage;
        this.weight = weight;

    }

    public String getIonicTech() {
        return ionicTech;
    }

    public void setIonicTech(String ionicTech) {
        this.ionicTech = ionicTech;
    }

    public String getWattage() {
        return wattage;
    }

    public void setWattage(String wattage) {
        this.wattage = wattage;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HairDryer hairDryer = (HairDryer) o;
        return ionicTech.equals(hairDryer.ionicTech) && wattage.equals(hairDryer.wattage) && weight.equals(hairDryer.weight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ionicTech, wattage, weight);
    }

    @Override
    public String toString() {
        return "\nDrier \uD83D\uDD3B\n"+
                "ID => " + super.getId() + "\n" +
                "Name => " + super.getName() + "\n" +
                "Brand => " + super.getBrand() + "\n" +
                "Quantity => " + super.getQuantity() + "\n" +
                "Ionic Tech => " + ionicTech + "\n" +
                "Wattage => " + wattage + "\n" +
                "Weight => "  + weight + "\n" +
                "Price => " + super.getPrice();
    }
}
