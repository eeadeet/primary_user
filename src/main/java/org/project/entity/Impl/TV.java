package org.project.entity.Impl;


import java.util.Objects;

public class TV extends Products {
    public TV(int id, String name, String brand, int quantity, double price) {
        super(id, name, brand, quantity, price);
    }


    private String display;
    private String resolution;
    private String refresh_rate;
    private String screen_size;


    public TV(int id, String name, String brand, int quantity, String display, String resolution, String refresh_rate,String screen_size, double price) {
        super(id, name, brand, quantity, price);

        this.display = display;
        this.resolution = resolution;
        this.refresh_rate = refresh_rate;
        this.screen_size= screen_size;

    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getRefresh_rate() {
        return refresh_rate;
    }

    public void setRefresh_rate(String refresh_rate) {
        this.refresh_rate = refresh_rate;
    }

    public String getScreen_size() {
        return screen_size;
    }

    public void setScreen_size(String screen_size) {
        this.screen_size = screen_size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TV tv = (TV) o;
        return display.equals(tv.display) && resolution.equals(tv.resolution) && refresh_rate.equals(tv.refresh_rate) && screen_size.equals(tv.screen_size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(display, resolution, refresh_rate, screen_size);
    }

    @Override
    public String toString() {
        return "\nTV \uD83D\uDD3B\n"+
                "ID => " + super.getId() + "\n" +
                "Name => " + super.getName() + "\n" +
                "Brand => " + super.getBrand() + "\n" +
                "Quantity => " + super.getQuantity() + "\n" +
                "Display => " + display + "\n" +
                "Resolution => " + resolution + "\n" +
                "Refresh Rate => " + refresh_rate + "\n" +
                "Screen size => " + screen_size + "\n" +
                "Price => " + super.getPrice();
    }
}

