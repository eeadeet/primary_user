package org.project.entity.Impl;

import java.util.Objects;

public class Refrigerator extends Products {

    public Refrigerator(int id, String name, String brand, int quantity, double price) {
        super(id, name, brand, quantity, price);
    }


        private String capacity;
        private String product_dimensions;


    public Refrigerator(int id, String name, String brand, int quantity, String capacity, String product_dimensions, double price) {
            super(id, name, brand, quantity, price);

            this.capacity = capacity;
            this.product_dimensions = product_dimensions;

    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getProduct_dimensions() {
        return product_dimensions;
    }

    public void setProduct_dimensions(String product_dimensions) {
        this.product_dimensions = product_dimensions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Refrigerator that = (Refrigerator) o;
        return capacity.equals(that.capacity) && product_dimensions.equals(that.product_dimensions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, product_dimensions);
    }

    @Override
    public String toString() {
        return "\nRefrigerator \uD83D\uDD3B\n"+
                "ID => " + super.getId() + "\n" +
                "Name => " + super.getName() + "\n" +
                "Brand => " + super.getBrand() + "\n" +
                "Quantity => " + super.getQuantity() + "\n" +
                "Capacity => " + capacity + "\n" +
                "Product Dimensions => " + product_dimensions + "\n" +
                "Price => " + super.getPrice();
    }
}
