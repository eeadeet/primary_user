package org.project;

import org.project.admin.AdminMain;
import org.project.controller.Impl.ControllerImpl;
import org.project.service.Impl.*;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        // Create an instance of the controller
        AirConditionerService airConditionerService = new AirConditionerService();
        HairDryerService hairDryerService = new HairDryerService();
        RefrigeratorService refrigeratorService = new RefrigeratorService();
        TvService tvService = new TvService();
        VacuumCleanerService vacuumCleanerService = new VacuumCleanerService();
        ControllerImpl controller = new ControllerImpl(airConditionerService, hairDryerService, refrigeratorService, tvService, vacuumCleanerService);

        // Main application loop
        while (true) {
            System.out.println("\nMain Menu:");
            System.out.println("1. View Products");
            System.out.println("2. Admin Panel");
            System.out.println("3. Exit");

            System.out.print("Enter your choice: ");
            try (Scanner scanner = new Scanner(System.in)) {
                int choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        // View Products
                        controller.view();
                        break;
                    case 2:
                        // Admin Panel
                        AdminMain.adminMenu();  // Call the static method in AdminMain
                        break;
                    case 3:
                        System.out.println("Exiting. Goodbye!");
                        return; // This will exit the main method and the application
                    default:
                        System.out.println("Invalid choice. Please try again.");
                        break;
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
}
