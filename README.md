# Household Appliances Warehouse Management System

This Java console application, the Household Appliances Warehouse Management System, allows users to search for household appliances in a warehouse inventory using various parameters. It provides an efficient way to find products based on their name, ID, category, and other criteria. The system reads the inventory data from a pre-formatted text file and presents the search results and product details to help users make informed decisions.

## Project Information

- Application Name: Household Appliances Warehouse Management System
- Version: 1.0
- Creation Date: 22.05.23

## Developer Information

- Developer Name: Sardor
- Email Address: Sardor_Xabibullayev@student.itpu.uz

## Available Commands

The following commands are available for interacting with the Household Appliances Warehouse Management System:

1. `search` - Search for household appliances based on specified parameters
2. `list` - Display a list of all household appliances in the inventory
3. `exit` - Exit the application

## Usage

Upon launching the application, you will be presented with a command-line interface. Follow the prompts to input commands and parameters as needed.

### Searching for Household Appliances

To search for household appliances, use the `search` command followed by the desired parameters. The system will display a list of matching household appliances with their details.


### Listing Household Appliances

To display a list of all household appliances in the inventory, use the `list` command. The household appliances will be sorted according to the selected order.


### Exiting the Application

To exit the application, simply use the `exit` command.


## Error Handling

The Household Appliances Warehouse Management System handles errors gracefully and provides appropriate error messages to the user. If there are no matching household appliances for a search, a message indicating the absence of products will be displayed.

## Source Code

The source code for this project is hosted in a Git repository, which allows for version control and review of the project's progress over time. You can access the repository at [https://gitlab.com/wearenotrasist/household-appliances-warehouse.git].

## Testing

Thorough testing has been performed to ensure that the Household Appliances Warehouse Management System functions as expected and handles all edge cases. The application has been tested for input validation, error handling, and correct display of search results and product details.

## Feedback and Contributions

Feedback and contributions to the Household Appliances Warehouse Management System are welcome. If you encounter any issues or have suggestions for improvements, please feel free to contact the developer at the provided email address.
