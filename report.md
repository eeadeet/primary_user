|  #    | Stage                  | Start date |  End date  | Comment
||----------------------------------------------------------------------------------------------------------------
|| 1    | Task Clarification     | 01.05.2023 | 09.05.2023 |
|| 2    | Analyze                | 24.05.2023 | 09.06.2023 | Analyzed the file
|| 3    | Initial Commit         | 22.05.2023 | 22.05.2023 | Created the blank project
|| 4    | Software development   | 22.05.2023 | 05.06.2023 |
|| 5    | Main layer             | 22.05.2023 | 07.06.2023 | Added info of author and linked to controller
|| 6    | Dao layer              | 26.05.2023 | 02.06.2023 | Written code for reading csv file
|| 7    | Product layer          | 22.05.2023 | 29.05.2023 | Written design for output of products
|| 8    | Controller layer       | 22.05.2023 | 09.06.2023 | Written switch cases for using the code connecting to service
|| 9    | Services layer         | 07.05.2023 | 09.06.2023 | Written methods for every variables
|| 10   | Test                   | 09.06.2023 | 09.06.2023 | Written test for find out if everything works correctly
|| 11   | Presentation           | 09.06.2023 | 09.06.2023 |